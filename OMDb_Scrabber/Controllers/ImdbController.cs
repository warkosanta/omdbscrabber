﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System;
using Saritasa.Tools.Messages.Abstractions;
using System.Threading.Tasks;
using OMDbScrabberLibrary.MoviesContext;

namespace OMDbScrabber.Controllers
{
    /// <summary>
    /// MVC Controller.
    /// </summary>
    public class ImdbController : Controller
    {
        private readonly IMessagePipelineService pipelineService;

        /// <summary>
        /// Constructor with arguments.
        /// </summary>
        public ImdbController(IMessagePipelineService pipelineService)
        {
            this.pipelineService = pipelineService ?? throw new ArgumentNullException(nameof(pipelineService));
        }

        /// <summary>
        /// First page displayed entry and button.
        /// </summary>
        public IActionResult Upload()
        {
            return View();
        }

        /// <summary>
        /// Second page displaed sorted movies.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> List(decimal ratingAbove = 0, int runtimeMinsAbove = 0, int runtimeMinsBelow = 0, string hasActor = "")
        {
            ViewBag.RatingAbove = ratingAbove;
            ViewBag.RuntimeMinsAbove = runtimeMinsAbove;
            ViewBag.RuntimeMinsBelow = runtimeMinsBelow;
            ViewBag.HasActor = hasActor;

            ViewBag.Movies = pipelineService.Query<ImdbQueries>().With(q => q.Search(ratingAbove, runtimeMinsAbove, runtimeMinsBelow, hasActor));
            return View();
        }

        /// <summary>
        /// Second page displayed unsorted movies.
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> List(string ids)
        {
            var command = new ImportImdbMoviesInfoCommand()
            {
                ImdbIds = ids.Split(',').Select(i => i.Trim()).ToArray()
            };
            await pipelineService.HandleCommandAsync(command);
            return Redirect("List");
        }
    }
}
