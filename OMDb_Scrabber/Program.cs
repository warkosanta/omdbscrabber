﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace OMDbScrabber
{
    /// <summary>
    /// Class with application start point.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Enter method.
        /// </summary>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
