﻿using Microsoft.EntityFrameworkCore;
using OMDbScrabberLibrary.Entities;
using OMDbScrabberLibrary.Interfaces;

namespace OMDbScrabber.DataContexts
{
    /// <summary>
    /// DbContext for movies and actors.
    /// </summary>
    public class MovieContext : DbContext, IMovieContext
    {
        /// <summary>
        /// Set of movies data.
        /// </summary>
        public DbSet<Movie> Movies { get; set; }

        /// <summary>
        /// Set of actors data.
        /// </summary>
        public DbSet<Actor> Actors { get; set; }

        /// <summary>
        /// Set of data to manage many-to-many bond between actors and movies.
        /// </summary>
        public DbSet<MovieActors> MovieActors { get; set; }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public MovieContext()
        {
        }

        /// <summary>
        /// Constructor with option.
        /// </summary>
        public MovieContext(DbContextOptions<MovieContext> options) : base(options)
        {
        }
    }
}
