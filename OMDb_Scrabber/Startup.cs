using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OMDbScrabber.DataContexts;
using OMDbScrabberLibrary.Domain;
using OMDbScrabberLibrary.Entities;
using OMDbScrabberLibrary.Interfaces;
using Saritasa.Tools.Messages.Abstractions;
using Saritasa.Tools.Messages.Commands;
using Saritasa.Tools.Messages.Common;
using Saritasa.Tools.Messages.Queries;
using System.Collections.Generic;
using System.Reflection;

namespace OMDbScrabber
{
    /// <summary>
    /// Class with application settings.
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Set settings for application using DI.
            services.Configure<ImdbConfiguration>(Configuration.GetSection("ImdbSettings"));
            services.AddDbContext<MovieContext>(options => options.UseSqlServer(Configuration.GetConnectionString("MovieDatabase")));
            services.AddTransient<IMovieContext, MovieContext>();
            services.AddTransient<MovieParser>();
            services.AddTransient<MovieManager>();

            var pipelinesContainer = new DefaultMessagePipelineContainer();
            pipelinesContainer.AddCommandPipeline().AddStandardMiddlewares(options =>
            {
                options.Assemblies = new List<Assembly> { typeof(Actor).Assembly };
            });
            pipelinesContainer.AddQueryPipeline().AddStandardMiddlewares();
            services.AddSingleton<IMessagePipelineContainer>(pipelinesContainer);
            services.AddScoped<IMessagePipelineService, DefaultMessagePipelineService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Set route for start page.
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Imdb}/{action=Upload}");
            });
        }
    }
}
