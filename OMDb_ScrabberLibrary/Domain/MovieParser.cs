﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using OMDbScrabberLibrary.Entities;

namespace OMDbScrabberLibrary.Domain
{
    /// <summary>
    /// Class with methods for movie data parser.
    /// </summary>
    public class MovieParser : IMovieParser
    {
        /// <summary>
        /// Get collection of movie actors.
        /// </summary>
        /// <param name="Movie Json"></param>
        public IEnumerable<Actor> GetActors(string data)
        {
            _= data ?? throw new ArgumentNullException(nameof(data));
            List<Actor> actors = new List<Actor>();
            var jtmovie = JToken.Parse(data);
            foreach (var a in jtmovie["Actors"].ToString().Split(','))
            {
                actors.Add(new Actor { Name = a });
            }
            return actors;
        }

        /// <summary>
        /// Get an instance of movie.
        /// </summary>
        /// <param name="Movie Json"></param>
        public Movie GetMovie(string data)
        {
            var jtmovie = JToken.Parse(data);
            Movie movie = new Movie();
            movie.ImdbId = jtmovie["imdbID"].ToString();
            movie.Title = jtmovie["Title"].ToString();
            movie.Genre = jtmovie["Genre"].ToString().Equals("N/A") ? "" : jtmovie["Genre"].ToString();
            movie.ImdbRating = jtmovie["imdbRating"].ToString().Equals("N/A") ? 0 : Convert.ToDecimal(jtmovie["imdbRating"].ToString());
            movie.RuntimeMins = jtmovie["Runtime"].ToString().Equals("N/A") ? 0 : ParseMovieTimeToMinutes(jtmovie["Runtime"].ToString());
            movie.ReleaseDate = jtmovie["Released"].ToString().Equals("N/A") ? default : DateTime.Parse(jtmovie["Released"].ToString());
            return movie;
        }

        private int ParseMovieTimeToMinutes(string timespent)
        {
            int minutes = 0;

            var chars = timespent.Split(' ').ToList();
            for (int i = 0; i < chars.Count; i += 2)
            {
                if (chars[i + 1] == "h")
                {
                    minutes += Convert.ToInt32(chars[i]) * 60;
                }
                else if (chars[i + 1] == "min")
                {
                    minutes += Convert.ToInt32(chars[i]);
                }
            }
            return minutes;
        }
    }
}
