﻿using Microsoft.Extensions.Options;
using OMDbScrabberLibrary.Entities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OMDbScrabberLibrary.Domain
{
    /// <summary>
    /// Class to manage movies data.
    /// </summary>
    public class MovieManager : IMovieParser
    {
        private IRestClient client = new RestClient();
        private readonly ImdbConfiguration imdbConfiguration;
        private readonly IMovieParser parser;

        public MovieManager(IOptions<ImdbConfiguration> imdbConfiguration, MovieParser parser)
        {
            this.parser = parser ?? throw new ArgumentNullException(nameof(parser));
            this.imdbConfiguration = imdbConfiguration.Value ?? throw new ArgumentNullException(nameof(imdbConfiguration));
        }

        /// <summary>
        /// Get movie data from remote service.
        /// </summary>
        /// <param name="Array of imdb ids."></param>
        public async Task<List<string>> GetMoviesActorsJson(string[] movieIds)
        {
            _= movieIds ?? throw new ArgumentNullException(nameof(movieIds));
            RestRequest request;
            List<string> data = new List<string>();
            foreach (var id in movieIds)
            {
                request = new RestRequest(imdbConfiguration.BaseUrl + string.Format(imdbConfiguration.Address, id, imdbConfiguration.ImdbKey), Method.POST);
                var result = await client.ExecuteTaskAsync(request);
                data.Add(result.Content);
            }

            return data;
        }

        /// <summary>
        /// Parse JSON to movie using instance of MovieParse.
        /// </summary>
        public Movie GetMovie(string moviesActorsJson)
        {
            return parser.GetMovie(moviesActorsJson);
        }

        /// <summary>
        /// Parse JSON to actors collection using instance of MovieParse.
        /// </summary>
        public IEnumerable<Actor> GetActors(string moviesActorsJson)
        {
            return parser.GetActors(moviesActorsJson);
        }
    }
}
