﻿ namespace OMDbScrabberLibrary.Entities
{
    /// <summary>
    /// Class represents imdb connection data.
    /// </summary>
    public class ImdbConfiguration
    {
        /// <summary>
        /// Base URL of service.
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// API adress.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// User's unique key.
        /// </summary>
        public string ImdbKey { get; set; }
    }
}
