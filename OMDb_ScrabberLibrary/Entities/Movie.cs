﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OMDbScrabberLibrary.Entities
{
    /// <summary>
    /// Class represents movie.
    /// </summary>
    public class Movie
    {
        /// <summary>
        /// Movie Id in database.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// ImdbId of movie.
        /// </summary>
        [Required]
        public string ImdbId { get; set; }

        /// <summary>
        /// Title of movie.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Genre of movie.
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// Movie release date.
        /// </summary>
        public DateTime ReleaseDate { get; set; }

        /// <summary>
        /// Runtime of movie in munites.
        /// </summary>
        public int RuntimeMins { get; set; }

        /// <summary>
        /// Movie rating.
        /// </summary>
        public decimal ImdbRating { get; set; }

        /// <summary>
        /// Date of Movie instance creation.
        /// </summary>
        public DateTime CreateAt { get; set; } = DateTime.Now;

        /// <summary>
        /// Collection of movie actors.
        /// </summary>
        public List<MovieActors> MovieActors { get; set; } = new List<MovieActors>();
    }
}
