﻿using System.Collections.Generic;

namespace OMDbScrabberLibrary.Entities
{
    /// <summary>
    /// Entity of actor.
    /// </summary>
    public class Actor
    {
        /// <summary>
        /// Actor Id in database.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of Actor.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Collection of movie actors.
        /// </summary>
        public List<MovieActors> MovieActors { get; private set; } = new List<MovieActors>();
    }
}
