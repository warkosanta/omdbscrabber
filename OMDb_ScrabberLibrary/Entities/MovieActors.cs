﻿namespace OMDbScrabberLibrary.Entities
{
    /// <summary>
    /// Entity to manage many-to-many bond between actors and movies.
    /// </summary>
    public class MovieActors
    {
        /// <summary>
        /// MovieActors Id in database.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Movie Id.
        /// </summary>
        public int MovieId { get; set; }

        /// <summary>
        /// Movie instance.
        /// </summary>
        public Movie Movie { get; set; }

        /// <summary>
        /// Actor Id.
        /// </summary>
        public int ActorId { get; set; }

        /// <summary>
        /// Actor instance.
        /// </summary>
        public Actor Actor { get; set; }
    }
}
