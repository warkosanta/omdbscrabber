﻿using OMDbScrabberLibrary.Domain;
using OMDbScrabberLibrary.Entities;
using Saritasa.Tools.Messages.Abstractions.Commands;
using System;
using System.Threading.Tasks;
using System.Linq;
using OMDbScrabberLibrary.Interfaces;

namespace OMDbScrabberLibrary.MoviesContext
{
    /// <summary>
    /// Command handler.
    /// </summary>
    [CommandHandlers]
    public class ImdbMoviesCommandHandler
    {
        private readonly IMovieContext movieContext;
        private readonly MovieManager movieManager;

        public ImdbMoviesCommandHandler(IMovieContext context, MovieManager movieManager)
        {
            movieContext = context ?? throw new ArgumentNullException(nameof(context));
            this.movieManager = movieManager ?? throw new ArgumentNullException(nameof(movieManager));
        }

        /// <summary>
        /// Get movie data from remote service.
        /// </summary>
        public async Task HandleListCommand(ImportImdbMoviesInfoCommand command)
        {
            foreach (var id in command.ImdbIds)
            {
                // If there is no films with current id, append it into databse.
                if (!movieContext.Movies.Any(movie => movie.ImdbId == id))
                {
                    // Get JSON from remote service.
                    var movieJson = await movieManager.GetMoviesActorsJson(command.ImdbIds);
                    movieJson.ForEach(async movie =>
                    {
                        if (!movie.Contains("False"))
                        {
                            // Parse JSON to movie.
                            var newMovie = movieManager.GetMovie(movie);

                            // Get actors from JSON.
                            var newActors = movieManager.GetActors(movie);

                            // Add movie and it's actors into database.
                            await movieContext.Actors.AddRangeAsync(newActors);
                            await movieContext.Movies.AddAsync(newMovie);
                            movieContext.SaveChanges();

                            // Create relationship between movie and actor.
                            foreach (var actor in newActors)
                            {
                                movieContext.MovieActors.Add(new MovieActors { ActorId = actor.Id, MovieId = newMovie.Id });
                            }
                        }
                    });
                    movieContext.SaveChanges();
                }
                else
                {
                    command.ImdbIds = command.ImdbIds.Where(imdbId => !imdbId.Equals(id)).ToArray();
                }
            }
        }
    }
}