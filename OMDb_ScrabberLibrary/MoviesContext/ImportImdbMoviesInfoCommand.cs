﻿namespace OMDbScrabberLibrary.MoviesContext
{
    /// <summary>
    /// Command with IMDB's id's.
    /// </summary>
    public class ImportImdbMoviesInfoCommand
    {
        /// <summary>
        /// Array with IMDB's id's.
        /// </summary>
        public string[] ImdbIds { get; set; }
    }
}
