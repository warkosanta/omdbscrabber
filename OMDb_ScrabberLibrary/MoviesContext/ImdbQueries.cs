﻿using OMDbScrabberLibrary.Dtos;
using OMDbScrabberLibrary.Entities;
using OMDbScrabberLibrary.Interfaces;
using Saritasa.Tools.Messages.Abstractions.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OMDbScrabberLibrary.MoviesContext
{
    /// <summary>
    ///  Query that performs search within database.
    /// </summary>
    [QueryHandlers]
    public class ImdbQueries
    {
        private List<MovieActorsDto> movieDto = new List<MovieActorsDto>();

        private readonly IMovieContext movieContext;

        public ImdbQueries(IMovieContext movieContext)
        {
            this.movieContext = movieContext ?? throw new ArgumentNullException(nameof(movieContext));
        }

        /// <summary>
        /// Search method.
        /// </summary>
        public List<MovieActorsDto> Search(decimal ratingAbove, int runtimeMinsAbove, int runtimeMinsBelow, string hasActor)
        {
            var movies = movieContext.Movies.AsQueryable();

            if (ratingAbove != 0)
            {
                movies = movies.Where(movie => movie.ImdbRating >= ratingAbove);
            }

            if (runtimeMinsAbove != 0)
            {
                movies = movies.Where(movie => movie.RuntimeMins >= runtimeMinsAbove);
            }

            if (runtimeMinsBelow != 0)
            {
                movies = movies.Where(movie => movie.RuntimeMins <= runtimeMinsBelow);
            }

            if (!string.IsNullOrEmpty(hasActor))
            {
                movies = movies.Where(movie =>
                    movieContext.MovieActors.Any(movieActors => movieActors.MovieId == movie.Id
                        && movieContext.Actors.FirstOrDefault(actor => actor.Id == movieActors.ActorId).Name.Contains(hasActor)));
            }

            foreach (Movie movie in movies.ToList())
            {
                var actorsIds = movieContext.MovieActors.Where(movieActors => movieActors.MovieId == movie.Id).Select(movieActors => movieActors.ActorId).ToList();
                var actors = movieContext.Actors.Where(actor => actorsIds.Contains(actor.Id)).ToList();

                movieDto.Add(new MovieActorsDto
                {
                    Movie = movie,
                    Actors = actors
                });
            }

            return movieDto;
        }
    }
}
