﻿using OMDbScrabberLibrary.Entities;
using System.Collections.Generic;

namespace OMDbScrabberLibrary.Dtos
{
    /// <summary>
    /// DTO for movie and its actors.
    /// </summary>
    public class MovieActorsDto
    {
        /// <summary>
        /// Movie.
        /// </summary>
        public Movie Movie { get; set; }

        /// <summary>
        /// Movie actors.
        /// </summary>
        public List<Actor> Actors { get; set; }
    }
}
