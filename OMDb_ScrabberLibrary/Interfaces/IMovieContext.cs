﻿using OMDbScrabberLibrary.Entities;
using Microsoft.EntityFrameworkCore;

namespace OMDbScrabberLibrary.Interfaces
{
    public interface IMovieContext
    {
        /// <summary>
        /// Set of movies data.
        /// </summary>
        DbSet<Movie> Movies { get; set; }

        /// <summary>
        /// Set of actors data.
        /// </summary>
        DbSet<Actor> Actors { get; set; }

        /// <summary>
        /// Set of data to manage many-to-many bond between actors and movies.
        /// </summary>
        DbSet<MovieActors> MovieActors { get; set; }

        /// <summary>
        /// Save changes.
        /// </summary>
        int SaveChanges();
    }
}
