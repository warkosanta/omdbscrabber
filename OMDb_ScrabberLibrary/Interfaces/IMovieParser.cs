﻿using OMDbScrabberLibrary.Entities;
using System.Collections.Generic;

namespace OMDbScrabberLibrary.Domain
{
    /// <summary>
    /// Interface for movie data parsing.
    /// </summary>
    public interface IMovieParser
    {
        Movie GetMovie(string data);

        IEnumerable<Actor> GetActors(string data);
    }
}
